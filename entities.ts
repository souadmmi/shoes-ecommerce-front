export interface Categorie {
    id?: number ,
    produits?: number ,
    name: string 
}

export interface Image{

    id?:number ,
    url:string ,
    produit?:Produit 
}

export interface Commande {
    id?: number ,
    date: string ,
    prix: number ,
    panier?: number ,
    user?: number 
}

export interface LignePanier {
    id?: number ,
    quantity: number ,
    prix?: number ,
    taille: Taille
    commande?: number 
}

export interface Note {
    id?: number ,
    content: string ,
    date: string ,
    produit?: Produit ,
    author?: User
}

export interface Produit {
    id?: number ,
    name: string ,
    description: string ,
    image: string ,
    prix: number ,
    marque: string ,
    categorie?: Categorie ,
    images?:Image[],
    taille?: Taille[],
    likes?:User[];

}

export interface Taille {
    id?: number ,
    taille: number ,
    stock: number ,
    panier?: number ,
    produit?: Produit
}

export interface User {
    id?: number ,
    email: string ,
    name: string ,
    password?: string ,
    role?: string ,
    notes?: number ,
    commandes?: number
}

export interface Page<T> {
    content: T[];
    totalPages: number;
    totalElements: number;
    size: number;
    number: number;
    first: boolean;
    last: boolean;
}