import type { UseFetchOptions } from "#app";
import { useAuth } from "./auth-service";


export function useApi<T>(
    url: string | (() => string),
    options: UseFetchOptions<T> = {}
) {
    return useFetch(url, {
        ...options,
        baseURL: 'http://localhost:8080',
        credentials: 'include',
        headers: useRequestHeaders(['cookie'])
    });
}

export const $api = $fetch.create({
    baseURL: 'http://localhost:8080',
    credentials: 'include',
    onRequest(context) {
        context.options.headers = {
            ...context.options.headers,
            'X-Requested-With': 'XMLHttpRequest',
            ...useRequestHeaders(['cookie'])
        }
    },
    onResponseError(context) {
        const route = useRoute();
        if (context.response.status == 401) {

            useAuth().value = null;
            navigateTo({
                path: '/login',
                query: { initialPage: route.path }
            });
        }
    }

})