import type { LignePanier, Taille } from "~/entities";


export const useCart = defineStore('cart', () => {
    const cartContent = useCookie<LignePanier[]>('cart', { sameSite: 'strict', default:() => [] });

    //pas sure
    // if (!cartContent.value) {
    //     cartContent.value = [];
    // }

    
    function addToCart(taille: Taille) {
        try {
            if (!cartContent.value) {
                console.error('cartContent.value is undefined');
                console.log(cartContent.value);
                return;
            }
            const exists = cartContent.value.find(item => item.taille?.id == taille.id);
            if (exists) {
                exists.quantity++;
                console.log(cartContent.value);
            } else {
                cartContent.value.push({ prix: taille.produit?.prix, quantity: 1, taille: taille });
                console.log(cartContent.value);
            }
        } catch (error) {
            console.error('Error in addToCart:', error);
            console.log(cartContent.value);

        }
    }
    

    function removeFromCart(panier: LignePanier) {
        cartContent.value = cartContent.value.filter(item => item != panier);

    }

    watch(cartContent, () => {
        // console.log('Appel vers api rest pour mettre à jour le panier');
        // console.log('si avec vuejs, aussi mettre à jour le localStorage')
    }, { deep: true })

    return { addToCart, cartContent, removeFromCart };
});